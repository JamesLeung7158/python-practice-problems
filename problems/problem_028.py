# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.


def remove_duplicate_letters(s):

    if len(s) > 0:
        new_str = " "
        for x in s:
            if x not in new_str:
                new_str += x
                return new_str
    else:
        return s
